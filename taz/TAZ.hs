{-# LANGUAGE Strict #-}

module Main where

import Data.Array
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as B8
import System.Environment
import System.Console.GetOpt
import Text.Printf
import System.IO.Error

import Core
import TAZMachine
import TAZProgram

data Flags = Dump | NoInput deriving (Eq, Show)

main :: IO ()
main = do
  (flags, (fn, inf)) <- processArgs
  tm <- loadMachine fn
  mtm <- if NoInput `elem` flags then return (Right tm) else getInput tm inf
  case mtm of
    Left err -> ioError (userError err)
    Right itm -> do
      if Dump `elem` flags then dumpMemory itm else return ()
      let etm = runMachine itm
      if Dump `elem` flags then dumpMemory etm else return ()
      output etm
                   
processArgs :: IO ([Flags], (FilePath, FilePath))
processArgs = do
  name <- getProgName
  args <- getArgs
  let opts = [ Option ['d'] ["dump"] (NoArg Dump) "Dump memory before and after execution"
             , Option ['n'] ["noinput"] (NoArg NoInput) "Do not ask for input"]
      (flags, fn, errs) = getOpt Permute opts args
  if not (null errs)
         || null fn
         || ((length fn /= 1) && NoInput `elem` flags)
         || ((length fn /= 2) && not (NoInput `elem` flags))
  then ioError (userError (concat errs ++ usageInfo ("Usage: " ++ name ++ " [OPTION...] program [inputfile]") opts))
  else return (flags, (fn!!0, fn!!1))

loadMachine :: String -> IO TAZMachine
loadMachine fn = do
  etp <- load fn
  case etp of
    Left err -> ioError (userError ("Program " ++ fn ++ " cannot be loaded. (" ++ err ++ ")"))
    Right tp -> return (ready tp) 

getInput :: TAZMachine -> FilePath -> IO (Either String TAZMachine)
getInput tm inf = catchIOError action (\e -> return (Left "IO error"))
  where action = do i <- B.readFile inf
                    if B.null i then return (Right tm) 
                    else let inp = map (\l -> case words l !! 0 of
                                                "I" -> II $ read (words l !! 1)
                                                "F" -> IF $ read (words l !! 1)
                                       ) (lines (B8.unpack i))
                         in return $ Right (tm {inpList = inp})
  
dumpMemory :: TAZMachine -> IO ()
dumpMemory tm = do
  putStrLn "\n=== === === === === Memory dump === === === === === ==="
  mapM_ (\(i, tw) -> printf "| %02x | %s\n" i (show tw)) (assocs $ mem tm)
  putStrLn "+++ +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ +++\n"

runMachine :: TAZMachine -> TAZMachine
runMachine tm = if isHalt tm == True then tm else runMachine (execute tm)

output :: TAZMachine -> IO ()
output tm = do
  putStrLn "+++ +++ +++ +++ +++ OUTPUT +++ +++ +++ +++ +++"
  mapM_ (\o -> case o of
                 OI int32 -> putStrLn ("Int32 " ++ show (int32)) 
                 OF float -> putStrLn ("Float " ++ show (float)) 
        ) (outList tm)
