module TAZProgram (
  load
, save
, isTAZProgram
) where

import Prelude hiding (empty, splitAt, length, concat)
import Imports
import Core


splitby4 :: ByteString -> [ByteString]
splitby4 bs = if (bs == empty) then []
              else let (p, s) = splitAt 4 bs
                   in p:splitby4 s


isTAZProgram :: FilePath -> IO Bool
isTAZProgram fp = catchIOError action (\e -> return False)
  where action = do h <- openBinaryFile fp ReadMode
                    bs <- hGet h 4
                    hClose h
                    return $ bs == magicnumber


-- | Loads a TAZ program.
--   If an error happens (no file), or the magic number is not there, returns Nothing.
load :: FilePath -> IO (Either String TAZProgram)
load f = catchIOError action (\e -> return (Left "IO error"))
  where action = do h <- openBinaryFile f ReadMode
                    bs <- hGet h 1028
                    hClose h
                    let (magic:prog) = splitby4 bs
                    if magic /= magicnumber then return (Left "Bad magic")
                    else return $ Right $ zip [0..] [(TW b) | b <- prog, length b == 4]


-- | Saves a TAZ program.
--   If an error happens, returns False.
save :: TAZProgram -> FilePath -> IO Bool
save tp f = catchIOError action (\e -> return False)
  where action = do h <- openBinaryFile f WriteMode
                    let bs = magicnumber `append` concat (map (\(_, TW b) -> b) tp)
                    hPut h bs
                    hClose h
                    return True
