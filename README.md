**TAZ** (abbr. *Too eAZy*) is an low-level-like interpreter.
It is being made primarily for teaching purposes.

A program for TAZ is a sequence of 4-byte words, which can be instructions or values.
Maximum size of the program is 1KB -> 255 instructions and values.
TAZ instruction consists of an opcode and three args, each a byte long.
TAZ args represent an address of value.
TAZ value can be an 32-bit signed integer, or a float.
TAZ loads a code and its data in the same memory space, i.e *does not* distinguish between instructions and values.
Since there are 8 bits for an opcode, there are 256 possible opcodes for TAZ.


**BugsBunny** is a editor for TAZ programs. Currently builds only under Linux.

**LooneyToons** is an web app, combining TAZ and BugsBunny into one pack.
