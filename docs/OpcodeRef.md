
# TAZ opcode reference **v0.1.0.0**


## Input/Output

 **`Ini`** *`dest`* `_` `_`
 Gets a value from the input list, then stores them to `dest`.

 **`Oui`** *`src`* `_` `_`
 Outputs the coutents of `src` in Int32 representation.

 **`Inf`** *`dest`* `_` `_`
 Gets a value from the input list, then stores them to `dest`.

 **`Ouf`** *`src`* `_` `_`
 Outputs the coutents of `src` in Float representation.


## Control flow

 **`Mov`** *`src`* *`dest`* `_`
 Copies contents of `src` to `dest`.

 **`Swp`** *`arg1`* *`arg2`* `_`
 Swaps contents of `arg1` and `arg2`.

 **`Jmp`** *`dest`* `_` `_`
 Unconditional jump to `dest`.

 **`Jeq`** *`arg1`* *`arg2`* *`dest`*
 Jumps to `dest` when contents of `arg1` and contents of `arg2` are equal.
 `arg1` and `arg2` here and below are compared as Int32.

 **`Jne`** *`arg1`* *`arg2`* *`dest`*
 Jumps to `dest` when contents of `arg1` and contents of `arg2` are not equal.
 
 **`Jls`** *`arg1`* *`arg2`* *`dest`*
 Jumps to `dest` when contents of `arg1` are strictly less than contents of `arg2`.

 **`Jgs`** *`arg1`* *`arg2`* *`dest`*
 Jumps to `dest` when contents of `arg1` are strictly greater than contents of `arg2`.

 **`Jle`** *`arg1`* *`arg2`* *`dest`*
 Jumps to `dest` when contents of `arg1` are less or equal than contents of `arg2`.

 **`Jge`** *`arg1`* *`arg2`* *`dest`*
 Jumps to `dest` when contents of `arg1` are greater or equal than contents of `arg2`.


## Int32 arithmetic

 **`Add`** *`arg1`* *`arg2`* *`dest`*
 Adds contents of `arg1` and `arg2`, then puts the result to `dest`.

 **`Sub`** *`arg1`* *`arg2`* *`dest`*
 Substracts contents of `arg2` from contents of `arg1`, then puts the result to `dest`.

 **`Mul`** *`arg1`* *`arg2`* *`dest`*
 Multiplies contents of `arg1` by contents of `arg2`, then puts the result to `dest`.

 **`Div`** *`arg1`* *`arg2`* *`dest`*
 Divides contents of `arg1` by contents of `arg2`, then puts the quotient to `dest`.

 **`Mod`** *`arg1`* *`arg2`* *`dest`*
 Divides contents of `arg1` by contents of `arg2`, then puts the remainder to `dest`.


## Bitwise operations

 **`And`** *`arg1`* *`arg2`* *`dest`*
 Applies bitwise AND to contents of `arg1` and `arg2`, then puts the result to `dest`.

 **`Or`** *`arg1`* *`arg2`* *`dest`*
 Applies bitwise OR to contents of `arg1` and `arg2`, then puts the result to `dest`.

 **`Xor`** *`arg1`* *`arg2`* *`dest`*
 Applies bitwise XOR to contents of `arg1` and `arg2`, then puts the result to `dest`.

 **`Not`** *`arg`* *`dest`* `_`
 Negates bits in contents of `arg`, then puts the result to `dest`.

 **`Shf`** *`arg`* *`count`* *`dest`*
 Shifts bits of contents of `arg` by contents of `count`, then puts the result to `dest`.
 Negative contents of `count` correspond to right shift.
 Positive contents of `count` correspond to left shift.

 **`Rot`** *`arg`* *`count`* *`dest`*
 Rotates bits of contents of `arg` by contents of `count`, then puts the result to `dest`.
 Negative contents of `count` correspond to right rotation.
 Positive contents of `count` correspond to left rotation.


## Float arithmetic

 **`Fad`** *`arg1`* *`arg2`* *`dest`*
 Adds contents of `arg1` and `arg2`, then puts the result to `dest`.

 **`Fsu`** *`arg1`* *`arg2`* *`dest`*
 Substracts contents of `arg2` from contents of `arg1`, then puts the result to `dest`.

 **`Fmu`** *`arg1`* *`arg2`* *`dest`*
 Multiplies contents of `arg1` by contents of `arg2`, then puts the result to `dest`.

 **`Fdi`** *`arg1`* *`arg2`* *`dest`*
 Divides contents of `arg1` by contents of `arg2`, then puts the quotient to `dest`.


## Convertion operations

 **`Itf`** *`arg`* *`dest`* `_`
 Converts contents of `arg` to Float representation, then puts it to `dest`.

 **`Fti`** *`arg`* *`dest`* `_`
 Converts contents of `arg` to Int32 representation, then puts it to `dest`.


## Misc

 **`Nop`** `_` `_` `_`
 Does nothing.

 **`Hlt`** `_` `_` `_`
 Halts the machine.
