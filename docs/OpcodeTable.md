
# TAZ opcode table **v0.1.0.0**

| 0x__ | _0 | _1 | _2 | _3 | _4 | _5 | _6 | _7 | _8 | _9 | _a | _b | _c | _d | _e | _f |
| :----: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| 0_ | Nop | Ini | Oui | Inf | Ouf | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| 1_ | Mov | Swp | Jmp | Jeq | Jne | Jls | Jgs | Jle | Jge | --- | --- | --- | --- | --- | --- | --- |
| 2_ | Add | Sub | Mul | Div | Mod | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| 3_ | And | Or  | Xor | Not | Shf | Rot | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| 4_ | Fad | Fsu | Fmu | Fdi | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| 5_ | Itf | Fti | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| 6_ | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| 7_ | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| 8_ | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| 9_ | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| a_ | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| b_ | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| c_ | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| d_ | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| e_ | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| f_ | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | Hlt |
