var tazmachine = {}
var fr = new FileReader()

$(function(){
  initStorage();
  drawUI(false);
})

function initStorage() {
  if (sessionStorage.length = 0) {
    sessionStorage.setItem(0, [0,0,0,0]);
  }
}

function drawUI(exec) {
  $("body").empty()
  drawHeader(exec);
  drawMemory(exec);
}

function drawHeader(exec) {
  $body = $("body");
  $header = $("<header>")
  $body.append($header);

  $header.append("<button type='button' id='open'>Open</button>");
  $header.append("<input id='file-open' type='file' style='display: none;'></input>");

  $header.append("<button id='edit'>Edit</button>");
  $header.append("<button id='run'>Run</button>");

  $header.append("<a id='savelink' id='file-save'><button id='save'>Save</button></input>");

  $edit = $("#edit");
  $run = $("#run");
  $open = $("#open");
  $fopen = $("#file-open");
  $save = $("#save");
  $fsave = $("#file-save");

  if (exec) {
    $run.addClass("selected");
    $edit.addClass("notselected");
  } else {
    $run.addClass("notselected");
    $edit.addClass("selected");
  }

  $edit.click(function(){drawUI(false)});
  $run.click(function(){
    if (sessionStorage.length > 0) {
      $.post("/compile", JSON.stringify(storage2tp()), function(r){
        if (r.ok) {
          tazmachine = r.tm
          drawUI(true);
        } else {
          showMessage(r.error);      
        }
      });
    }
  });

  $open.click(function(){$fopen.trigger("click")});
  $save.click(function(){
    s=""
    $.each(storage2tp(), function (i, e) {
      $.each(e, function(i1, e1){
        s+=String.fromCharCode(e1); // FIX: corrupting bytes
      })
    })
    $("#savelink").prop("download", "tazp");
    $("#savelink").prop("href", "data:application/octet-stream,TAZ!"+s);
    $("#savelink").trigger("click");
  });
  $fopen.change(function(){
    f = $("#file-open").get(0).files[0];
    fr.onload = function(){
      $("#file-open").val("")
      r = fr.result;
      a = new Uint8Array(r);
      ar = Array.from(a)
      ntp = []
      for (i=0, j=ar.length; i < j; i+=4) {ntp[i/4] = ar.slice(i, i+4)}
      magic = ntp.splice(0, 1)
      if (magic[0].toString() != "84,65,90,33") {
        showMessage("Bad magic.")
      } else {
        tp2storage(Object(ntp));
        drawUI(false);
      }
    }
    fr.onerror = function(){showMessage("Can't open file.")};
    fr.readAsArrayBuffer(f);
  })
}


function drawMemory(exec) {
  if (exec) {
    $("body").append("<div id='imo'></div>");
    $imo = $("#imo");
    $input = $("<ul>", {id: 'input'});
    $memory = $("<ul>", {id: 'memory'});
    $memory.prop("width", "33%")
    $output = $("<ul>", {id: 'output'});
    $imo.append($input);
    $imo.append($memory);
    $imo.append($output);

    $addinput = $("<button>", {class: "add", text: "+"});
    $delinput = $("<button>", {class: "deleteIO", text: "-"});
    $input.append($addinput);
    $input.append($delinput);
    $.each(tazmachine.input, function(i, e) {
      $input.append(newIO(e));
    })
    $.each(storage2tp(), function(addr, tw) {
      $memory.append(newWord(addr, tw, true, tazmachine.addr));
    })
    $memory.append("<button id='step'>-></button>");
    $.each(tazmachine.output, function(i, e) {
      $output.append(newIO(e));
    })
    $addinput.click(function(){
      $input.append(newIO({tag: "II", contents: 0}));
      tazmachine.input.push({tag: "II", contents: 0})
    })
    $delinput.click(function(){
      $input.children('.io').first().remove()
      tazmachine.input.splice(0,1);
    })
    $("#step").click(function(){
      $.post("/step", JSON.stringify(tazmachine), function(r){
        tazmachine = r.tm;
        drawUI(true);
      })
    });
  } else {
    $("body").append("<ul id='memory'></ul>");
    $memory = $("#memory");
    $.each(storage2tp(), function(addr, tw) {
      $memory.append(newWord(addr, tw, false));
    })
    $memory.append("<button class='add'>+</button>");
    $(".add").click(function(){
      if (sessionStorage.length < 256) {
        $(".add").before(newWord(sessionStorage.length, [0,0,0,0], false));
        sessionStorage.setItem(sessionStorage.length, [0,0,0,0])
      }
    });
    if (sessionStorage.length == 256) {$("#add").prop("disabled", true)}

//    $memory.sortable({
//      axis: "y",
//      containment: "parent",
//      items: "> .word",
//      scroll: true,
//      update: function (event, ui) {} // TODO: change tazprogram on sort
//    })
    }
}

function newWord(addr, tw, exec, ip) {
  $newword = $("<li>", {class:'word', 'data-addr': addr});
  $index = $("<div>", {class: 'index', text: addr});
  if (exec) {val = tazmachine.memory[addr]} else {val = tw}
  $value = $("<div>", {class: 'value', text: convert(val, "instr")});
  $newword.append($index);
  $newword.append($value);

  $views = $("<div>", {class:'views'});
  $instr = $("<button>", {class:'instr vselected'});
  $int32 = $("<button>", {class:'int32 vnotselected'});
  $float = $("<button>", {class:'float vnotselected'});
  $views.append($instr);
  $views.append($int32);
  $views.append($float);
  $newword.append($views);

  $value = $(".word[data-addr='"+addr+"'] > .value")

  $instr.click(function(){
    $(".word[data-addr='"+addr+"'] > .views > .instr").toggleClass("vselected", true);
    $(".word[data-addr='"+addr+"'] > .views > .instr").toggleClass("vnotselected", false);
    $(".word[data-addr='"+addr+"'] > .views > .int32").toggleClass("vselected", false);
    $(".word[data-addr='"+addr+"'] > .views > .int32").toggleClass("vnotselected", true);
    $(".word[data-addr='"+addr+"'] > .views > .float").toggleClass("vselected", false);
    $(".word[data-addr='"+addr+"'] > .views > .float").toggleClass("vnotselected", true);
    if (exec) {
      $(".word[data-addr='"+addr+"'] > .value").text(convert(tazmachine.memory[addr], "instr"))      
    } else {
      $(".word[data-addr='"+addr+"'] > .value").text(convert(storage2tp()[addr], "instr"))      
    }
  });
  $int32.click(function(){
    $(".word[data-addr='"+addr+"'] > .views > .instr").toggleClass("vselected", false);
    $(".word[data-addr='"+addr+"'] > .views > .instr").toggleClass("vnotselected", true);
    $(".word[data-addr='"+addr+"'] > .views > .int32").toggleClass("vselected", true);
    $(".word[data-addr='"+addr+"'] > .views > .int32").toggleClass("vnotselected", false);
    $(".word[data-addr='"+addr+"'] > .views > .float").toggleClass("vselected", false);
    $(".word[data-addr='"+addr+"'] > .views > .float").toggleClass("vnotselected", true);
    if (exec) {
      $(".word[data-addr='"+addr+"'] > .value").text(convert(tazmachine.memory[addr], "int32"))      
    } else {
      $(".word[data-addr='"+addr+"'] > .value").text(convert(storage2tp()[addr], "int32"))      
    }
  });
  $float.click(function(){
    $(".word[data-addr='"+addr+"'] > .views > .instr").toggleClass("vselected", false);
    $(".word[data-addr='"+addr+"'] > .views > .instr").toggleClass("vnotselected", true);
    $(".word[data-addr='"+addr+"'] > .views > .int32").toggleClass("vselected", false);
    $(".word[data-addr='"+addr+"'] > .views > .int32").toggleClass("vnotselected", true);
    $(".word[data-addr='"+addr+"'] > .views > .float").toggleClass("vselected", true);
    $(".word[data-addr='"+addr+"'] > .views > .float").toggleClass("vnotselected", false);
    if (exec) {
      $(".word[data-addr='"+addr+"'] > .value").text(convert(tazmachine.memory[addr], "float"))      
    } else {
      $(".word[data-addr='"+addr+"'] > .value").text(convert(storage2tp()[addr], "float"))      
    }
  });
  if (exec && tazmachine.addr == addr && tazmachine.halt) {
      $index.addClass("halt")
  }
  if (exec && tazmachine.addr == addr) {
      $index.addClass("ip")
  }
  if (!exec) {
    $effects = $("<div>", {class:'effects'});
    $modify = $("<button>", {class:'modify'});
    $delete = $("<button>", {class:'delete'});
    $effects.append($modify);
    $effects.append($delete);
    $newword.append($effects);
    $modify.click(function(){
      tw = storage2tp()[addr];
      $b0 = $("<input>", {class: "byte", type:"number", "data-offset": 0, min: 0, max: 255, value: tw[0]});
      $b1 = $("<input>", {class: "byte", type:"number", "data-offset": 1, min: 0, max: 255, value: tw[1]});
      $b2 = $("<input>", {class: "byte", type:"number", "data-offset": 2, min: 0, max: 255, value: tw[2]});
      $b3 = $("<input>", {class: "byte", type:"number", "data-offset": 3, min: 0, max: 255, value: tw[3]});
      $value = $(".word[data-addr='"+addr+"'] > .value")      
      $value.text("")
      $value.append($b0);
      $value.append($b1);
      $value.append($b2);
      $value.append($b3);
      $(".word[data-addr='"+addr+"'] > .effects > .modify").off("click").prop("class", "apply");
      $(".word[data-addr='"+addr+"'] > .effects > .delete").off("click").prop("class", "cancel");
      $(".word[data-addr='"+addr+"'] > .views > .instr").toggleClass("vnotselected", true);
      $(".word[data-addr='"+addr+"'] > .views > .int32").toggleClass("vnotselected", true);
      $(".word[data-addr='"+addr+"'] > .views > .float").toggleClass("vnotselected", true);
      $(".word[data-addr='"+addr+"'] > .views > .instr").prop("disabled", true);
      $(".word[data-addr='"+addr+"'] > .views > .int32").prop("disabled", true);
      $(".word[data-addr='"+addr+"'] > .views > .float").prop("disabled", true);
      $(".word[data-addr='"+addr+"'] > .effects > .apply").click(function(){
        ntw = []
        ntw.push($(".word[data-addr='"+addr+"'] > .value > .byte[data-offset=0]").val())
        ntw.push($(".word[data-addr='"+addr+"'] > .value > .byte[data-offset=1]").val())
        ntw.push($(".word[data-addr='"+addr+"'] > .value > .byte[data-offset=2]").val())
        ntw.push($(".word[data-addr='"+addr+"'] > .value > .byte[data-offset=3]").val())
        sessionStorage.setItem(addr, ntw);
        drawUI(false)
      });
      $(".word[data-addr='"+addr+"'] > .effects > .cancel").click(function(){
        drawUI(false); // TODO: full redrawing is expensive, think about DOM manipulation
      });

    });
    $delete.click(function(){
      a = $.makeArray(sessionStorage);
      a.splice(addr, 1);
      tp2storage(Object(a));
      drawUI(false); // TODO: full redrawing is expensive, think about DOM manipulation
    });
  }

  return $newword;
}

function newIO(el) {
  $io = $("<li>", {class: "io"});
  $t = $("<div>", {class: "type"})
  $v = $("<div>", {class: "valueIO"})
  $io.append($t);
  $io.append($v);
  $v.text(el.contents);
  switch (el.tag) {
    case "II": {
      $io.addClass("input");
      $t.text("Int32");
      break;
    }
    case "IF": {
      $io.addClass("input");
      $t.text("Float");
      break;
    }
    case "OI": {
      $io.addClass("output");
      $t.text("Int32");
      break;
    }
    case "OF": {
      $io.addClass("output");
      $t.text("Float");
      break;
    }
  }
  return $io;
}

// Helpers

function tp2storage(tp) {
  sessionStorage.clear();
  $.each(tp, function(k, v){sessionStorage.setItem(k, v)});
}

function storage2tp() {
  tp = {};
  $.each(sessionStorage, function(k,v){
    a = v.split(",");
    $.each(a, function(i, e) {a[i]=parseInt(e)})
    tp[k] = a;
  });
  return tp;
}

function convert(tw, towhat) {
  var data = tw;
      buf = new ArrayBuffer(4);
      view = new DataView(buf);
  data.forEach(function(i, e){view.setUint8(e, i)})
  if (towhat == "instr") {
    return toOp(view.getUint8(0)) +' '+ view.getUint8(1) +' '+ view.getUint8(2) +' '+ view.getUint8(3)
  } else if (towhat == "int32") {
    return view.getInt32(0);
  } else if (towhat == "float") {
    return view.getFloat32(0);
  }
}

function showMessage(msg) {
  $msg = $("<div>", {class:"message"})
  $("body").prepend($msg);
  $msg.text(msg);
  $msg.click(function(){
    $msg.slideUp(500, function(){$msg.remove()});
  });
  $msg.slideDown(500);
}

function toOp(h) {
  switch (h) {
    case 0x00: return "NOP";
    case 0x01: return "INI";
    case 0x02: return "OUI";
    case 0x03: return "INF";
    case 0x04: return "OUF";
    case 0x10: return "MOV";
    case 0x11: return "SWP";
    case 0x12: return "JMP";
    case 0x13: return "JEQ";
    case 0x14: return "JNE";
    case 0x15: return "JLS";
    case 0x16: return "JGS";
    case 0x17: return "JLE";
    case 0x18: return "JGE";
    case 0x20: return "ADD";
    case 0x21: return "SUB";
    case 0x22: return "MUL";
    case 0x23: return "DIV";
    case 0x24: return "MOD";
    case 0x30: return "AND";
    case 0x31: return "OR ";
    case 0x32: return "XOR";
    case 0x33: return "NOT";
    case 0x34: return "SHF";
    case 0x35: return "ROT";
    case 0x40: return "FAD";
    case 0x41: return "FSU";
    case 0x42: return "FMU";
    case 0x43: return "FDI";
    case 0x50: return "ITF";
    case 0x51: return "FTI";
    case 0xff: return "HLT";
    default:   return "UNK";
  }
}
