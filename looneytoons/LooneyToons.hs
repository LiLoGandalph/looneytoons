{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE QuasiQuotes           #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE ViewPatterns          #-}
{-# LANGUAGE FlexibleInstances     #-}

module Main where

-- === App imports ===
import Core
import TAZMachine
import TAZProgram

-- === Qualified imports ===
import qualified Data.Text as T
import qualified Data.Array as A
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as B8

-- === Utility imports ===
import Data.Word
import Data.Maybe

-- === Web imports ===
import Data.Aeson
import Yesod.Core
import Yesod.EmbeddedStatic

-- === Application datatype ===
data LooneyToons = LooneyToons {getStatic :: EmbeddedStatic}

-- === JSON instances ===
instance ToJSON TAZMachine where
  toJSON (TAZM m a h i o) = object [ "memory" .= Yesod.Core.array (A.elems m) 
                                   , "addr" .= a
                                   , "halt" .= h
                                   , "input" .= i
                                   , "output" .= o
                                   ]

instance FromJSON TAZMachine where
  parseJSON (Object v) = TAZM
                       <$> v .: "memory"
                       <*> v .: "addr"
                       <*> v .: "halt"
                       <*> v .: "input"
                       <*> v .: "output"
instance ToJSON TAZWord where
  toJSON (TW bs) = Yesod.Core.array $ B.unpack bs
instance FromJSON TAZWord where
  parseJSON jsn = do
    l <- parseJSON jsn
    return $ TW $ B.pack l
instance FromJSON (A.Array Addr TAZWord) where
  parseJSON jsn = do
    l <- parseJSON jsn
    return $ A.listArray ((0, fromIntegral (length l)-1)::(Word8, Word8)) l

instance ToJSON TAZProgram where
  toJSON tp = let (addrs, tws) = unzip tp
                        in object $ zipWith (.=) (map (T.pack.show) addrs) tws
instance FromJSON TAZProgram where
  parseJSON (Object v) = do
-- TODO: Think harder about more general instance implementation
    vs <- mapM (v .:?) (map (T.pack.show) [0..255])
    return $ zip [0..255] (catMaybes vs)

instance ToJSON TAZInput
instance FromJSON TAZInput
instance ToJSON TAZOutput
instance FromJSON TAZOutput
instance ToJSON Instr
instance FromJSON Instr
instance ToJSON Opcode
instance FromJSON Opcode


-- === Templates for embedded static, and routing ===
mkEmbeddedStatic False "static" [embedDir "looneytoons/static"]
mkYesod "LooneyToons" [parseRoutes|
/         HomeR   GET
/static   StaticR EmbeddedStatic getStatic

/compile  CompR   POST
/step     StepR   POST
|]

-- === Main web instance ===
instance Yesod LooneyToons where
   addStaticContent = embedStaticContent getStatic StaticR Right
   defaultLayout contents = do
     PageContent title headTags bodyTags <- widgetToPageContent contents
     withUrlRenderer [hamlet|
     $doctype 5
     <html>
       <head>
         <link rel="icon" href=@{StaticR favicon_ico} sizes="64x64" type="image/png">
         <link rel="stylesheet" type="text/css" href=@{StaticR css_body_css}>
         <link rel="stylesheet" type="text/css" href=@{StaticR css_header_header_css}>
         <link rel="stylesheet" type="text/css" href=@{StaticR css_header_open_css}>
         <link rel="stylesheet" type="text/css" href=@{StaticR css_header_edit_css}>
         <link rel="stylesheet" type="text/css" href=@{StaticR css_header_run_css}>
         <link rel="stylesheet" type="text/css" href=@{StaticR css_header_save_css}>
         <link rel="stylesheet" type="text/css" href=@{StaticR css_imo_imo_css}>
         <link rel="stylesheet" type="text/css" href=@{StaticR css_imo_step_css}>
         <link rel="stylesheet" type="text/css" href=@{StaticR css_imo_input_css}>
         <link rel="stylesheet" type="text/css" href=@{StaticR css_imo_output_css}>
         <link rel="stylesheet" type="text/css" href=@{StaticR css_imo_io_css}>
         <link rel="stylesheet" type="text/css" href=@{StaticR css_imo_type_css}>
         <link rel="stylesheet" type="text/css" href=@{StaticR css_imo_valueIO_css}>
         <link rel="stylesheet" type="text/css" href=@{StaticR css_imo_deleteIO_css}>
         <link rel="stylesheet" type="text/css" href=@{StaticR css_memory_memory_css}>
         <link rel="stylesheet" type="text/css" href=@{StaticR css_memory_add_css}>
         <link rel="stylesheet" type="text/css" href=@{StaticR css_memory_word_word_css}>
         <link rel="stylesheet" type="text/css" href=@{StaticR css_memory_word_index_css}>
         <link rel="stylesheet" type="text/css" href=@{StaticR css_memory_word_value_css}>
         <link rel="stylesheet" type="text/css" href=@{StaticR css_memory_word_byte_css}>
         <link rel="stylesheet" type="text/css" href=@{StaticR css_memory_word_views_views_css}>
         <link rel="stylesheet" type="text/css" href=@{StaticR css_memory_word_views_instr_css}>
         <link rel="stylesheet" type="text/css" href=@{StaticR css_memory_word_views_int32_css}>
         <link rel="stylesheet" type="text/css" href=@{StaticR css_memory_word_views_float_css}>
         <link rel="stylesheet" type="text/css" href=@{StaticR css_memory_word_effects_effects_css}>
         <link rel="stylesheet" type="text/css" href=@{StaticR css_memory_word_effects_modify_css}>
         <link rel="stylesheet" type="text/css" href=@{StaticR css_memory_word_effects_delete_css}>
         <link rel="stylesheet" type="text/css" href=@{StaticR css_memory_word_effects_apply_css}>
         <link rel="stylesheet" type="text/css" href=@{StaticR css_memory_word_effects_cancel_css}>
         <script src=@{StaticR js_jquery_3_2_1_min_js}></script>
         <script src=@{StaticR js_jquery_ui_min_js}></script>
         <title>#{title}
         ^{headTags}
       <body>
         ^{bodyTags}
     |]


-- ======== MAIN =======
main :: IO ()
main = warp 3000 $ LooneyToons static

-- ====== Handlers ======

-- ======== Root ========
getHomeR :: Handler Html
getHomeR = defaultLayout $ do
  setTitle "LooneyToons"
  addScript $ StaticR js_looneytoons_js

-- ==== TAZ methods  ====
postCompR :: Handler Value
postCompR = do
  tp <- requireJsonBody :: Handler TAZProgram
  let tm = ready tp
  returnJson $ object ["ok" .= True, "tm" .= tm]

postStepR :: Handler Value
postStepR = do
  ctm <- requireJsonBody :: Handler TAZMachine
  let ntm = execute ctm
  returnJson $ object ["ok" .= True, "tm" .= ntm]

